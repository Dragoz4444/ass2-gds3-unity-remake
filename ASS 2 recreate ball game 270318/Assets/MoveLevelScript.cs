﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveLevelScript : MonoBehaviour {

    
    
    void Start()
    {
     
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.W) == true)
        {
            transform.Rotate(Vector3.left);
        }
        if (Input.GetKey(KeyCode.S) == true)
        {
            transform.Rotate(Vector3.right);
        }
        if (Input.GetKey(KeyCode.A) == true)
        {
            transform.Rotate(Vector3.down);
        }
        if (Input.GetKey(KeyCode.D) == true)
        {
            transform.Rotate(Vector3.up);
        }
    }
}
