﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camer_aFollowsBallScript : MonoBehaviour
{

    public GameObject playersphere;

    private Vector3 offset;

    void Start()
    {
        offset = transform.position - playersphere.transform.position;
    }

    void LateUpdate()
    {
        transform.position = playersphere.transform.position + offset;
    }
}
