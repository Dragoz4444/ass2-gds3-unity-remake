﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveLevelScript : MonoBehaviour {

    public float rotateSpeed;

    private Vector3 rotateDirection;
    
    
    void Start()
    {
        
    }

    void Update()
    {
        //new smort code for good and cool people to do it
        rotateDirection = new Vector3(Input.GetAxis("Vertical"), 0, Input.GetAxis("Horizontal"));
        rotateDirection *= rotateSpeed;

        transform.Rotate(rotateDirection);

        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.identity, 0.03f);
        
        
        /* old shitty code that was a way for babies to do it
        if (Input.GetKey(KeyCode.W) == true)
        {
            transform.Rotate(Vector3.left);
        }
        if (Input.GetKey(KeyCode.S) == true)
        {
            transform.Rotate(Vector3.right);
        }
        if (Input.GetKey(KeyCode.A) == true)
        {
            transform.Rotate(Vector3.down);
        }
        if (Input.GetKey(KeyCode.D) == true)
        {
            transform.Rotate(Vector3.up);
        }
        */

    }
}
