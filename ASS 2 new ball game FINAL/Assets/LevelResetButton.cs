﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelResetButton : MonoBehaviour {

	void Update () {

		if (Input.GetKey (KeyCode.Tab) == true) {
			transform.position = new Vector3 (0, 95, 0);
			GetComponent<Rigidbody> ().velocity = Vector3.zero;
		}

	}
}
