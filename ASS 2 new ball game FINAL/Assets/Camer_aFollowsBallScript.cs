﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camer_aFollowsBallScript : MonoBehaviour
{

	public Transform lookAt;
	public Transform camTransform;
	public float distance = 10.0f;
	public float mouseSpeed = 0;

	private float currentX = 0.0f;
	private float currentY = 45.0f;
	private const float yMin = 0.0f;
	private const float yMax = 50.0f;

	void Start()
	{
		camTransform = transform;
	}

	void Update()
	{
		currentX += (Input.GetAxis("Mouse X") / mouseSpeed);
		 currentY += (Input.GetAxis("Mouse Y") / mouseSpeed);

		currentY = Mathf.Clamp(currentY, yMin, yMax);
	}

	void LateUpdate()
	{
		Vector3 dir = new Vector3(0, 0, -distance);
		Quaternion rotation = Quaternion.Euler(currentY, currentX, 0);
		camTransform.position = lookAt.position + rotation * dir;
		camTransform.LookAt(lookAt.position);
	}
}