﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


    public class levelRestart : MonoBehaviour
    {
        public float threshold;

    void FixedUpdate()
    {
        if (transform.position.y < threshold)
        {
            transform.position = new Vector3(0, 0, 0);
            GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
    }
    }
