﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class levelChange : MonoBehaviour {
    public GameObject[] levelChangeArray;
    public int currentLevel;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    void OnCollisionEnter(Collision col)
    {
        if (col.collider.tag == "Finish"){
            Debug.Log("hello i am collide");
            levelChangeArray[currentLevel].gameObject.SetActive(false);
            currentLevel++;
            levelChangeArray[currentLevel].gameObject.SetActive(true);
            transform.position = new Vector3(0, 0, 0);
            GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
    }
}
