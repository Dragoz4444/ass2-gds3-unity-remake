﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Camer_aMouseMovementScript : MonoBehaviour
{
    public float horizontalSpeed = 2.0F;
    public float verticalSpeed = 2.0F;
    public GameObject playersphere;
    private Vector3 offset;
    
    void Start()
    {
        offset = transform.position - playersphere.transform.position;
    }
    void Update()
    {
        // Rotate the camera every frame so it keeps looking at the target
        this.transform.RotateAround(Vector3.zero, Vector3.up, Input.GetAxis("Mouse X"));
    }

    
}