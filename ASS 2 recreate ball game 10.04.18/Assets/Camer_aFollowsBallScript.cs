﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camer_aFollowsBallScript : MonoBehaviour
{

    public GameObject playersphere;

    private Vector3 offset;
    //static float t = 0.0f;
    public float minAngle = 0.0F;
    public float maxAngle = 90.0F;

    void Start()
    {
        offset = transform.position - playersphere.transform.position;
    }

    void LateUpdate()
    {
        float angle = Mathf.LerpAngle(minAngle, maxAngle, Time.time);
        transform.position = playersphere.transform.position;
        this.transform.RotateAround(Vector3.zero, Vector3.up, Input.GetAxis("Mouse X"));
        this.transform.RotateAround(Vector3.zero, Vector3.up, Input.GetAxis("Mouse Y"));
        //LerpAngle(0, 75, transform.position);

        
        //transform.eulerAngles = new Vector3(0, angle, 0);
    }
        //this.transform.RotateAround(Vector3.zero, Vector3.left, Input.GetAxis(Mathf.LerpAngle(0, 75, 1))); "Mouse Y"; }
 }

//Mathf.Lerp(minimum, maximum, t) public static float LerpAngle(float a, float b, float t);
